﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car();
            car1.Make = "Oldsmobile";
            car1.Model = "Cutlas Supreme";
            //Console.WriteLine(car1.ToString());

            Car car2 = new Car();
            car2.Make = "Geo";
            car2.Model = "Prism";
            //Console.WriteLine(car2.ToString());

            Car car3 = new Car();
            car3.Make = "Pontiac";
            car3.Model = "Firebird";
            //Console.WriteLine(car3.ToString());

            Book b1 = new Book();
            b1.Author = "Robert Tabor";
            b1.Title = "Microsoft .NET XML Web Services";
            b1.ISBN = "0-000-00000-0";
            //Console.WriteLine(b1.ToString());

            Book b2 = new Book();
            b2.Author = "An Author";
            b2.Title = "C# For dummies";
            b2.ISBN = "0-000-00000-0";
            //Console.WriteLine(b2.ToString());

            Book b3 = new Book();
            b3.Author = "Another Author";
            b3.Title = "Algorythms for dummies";
            b3.ISBN = "0-000-00000-0";
            //Console.WriteLine(b3.ToString());


            // Print stuff using arrayList
            ArrayList jeremyslist = new ArrayList();
            jeremyslist.Add(car1);
            jeremyslist.Add(car2);
            jeremyslist.Add(car3);

            printCarList(jeremyslist);

            Console.WriteLine();

            //Print stuff using Sorted Dictionary
            SortedDictionary<int, Car> carsorteddictionary = new SortedDictionary<int, Car>();
            carsorteddictionary.Add(1, car1);
            carsorteddictionary.Add(2, car2);
            carsorteddictionary.Add(3, car3);
            
            printCarDict(carsorteddictionary);

            Console.WriteLine();

            //Print stuff using SortedList
            SortedList jeremyssortedlist = new SortedList();
            jeremyssortedlist.Add(11, car1);
            jeremyssortedlist.Add(22, car2);
            jeremyssortedlist.Add(33, car3);

            printCarSL(jeremyssortedlist);

            Console.ReadLine();
        }

        //Print Cars in SortedList method 
        public static void printCarSL(SortedList printcsl)
        {
            ICollection keys = printcsl.Keys;
            foreach (int a in keys)
                Console.WriteLine(a + " " + printcsl[a]);
        }




        //Print Cars in ArrayList method 
        public static void printCarList(ArrayList printcars)
        {
            foreach (Car a in printcars)
                Console.WriteLine(a.ToString());
        }

        //Print Cars in SortedDictionary method 
        public static void printCarDict(SortedDictionary<int, Car> printcd)
        {
            foreach (KeyValuePair<int, Car> pair in printcd)
                Console.WriteLine("{0},{1}", pair.Key, pair.Value);
        }

 
    }

    class Car
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public override string ToString()
        {
            return Make + " " + Model;
        }
    }


    class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public override string ToString()
        {
            return Title + " " + Author + " " + ISBN;
        }
    }

}
