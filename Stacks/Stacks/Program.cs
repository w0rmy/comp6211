﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;  //Necessary for Stack

namespace Stacks
{
    class Program
    {
        static void Main(string[] args)
        {

            Stack bob = new Stack();

            Console.WriteLine("Stuff the stack:");
            
            var input = Console.ReadLine();
            foreach (var z in input)
            {
                bob.Push(z);
            }
            Console.WriteLine("Current stack is:");
            foreach (var x in bob)
            {
                Console.Write(x);
            }
            Console.ReadKey();
        }
    }
}
