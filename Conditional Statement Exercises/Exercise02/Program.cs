﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise02
{
    class Program
    {
        static void Main(string[] args)
        {
            int n1;
            int n2;
            int n3;

            Console.WriteLine("Input the 1st number:");
            n1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Input the 2nd number:");
            n2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Input the 3rd number:");
            n3 = int.Parse(Console.ReadLine());

            var ray = new int[] { n1, n2, n3 };
            Array.Sort(ray);

            Console.WriteLine($"Your highest number is: {ray[2]}");
            Console.ReadKey();
        }
    }
}
