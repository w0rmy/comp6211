﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditional_Statement_Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            int n1 = int.Parse(Console.ReadLine());

            if (n1 >= 0)
            {
                Console.WriteLine("Your number is positive");
            }
    
            else
            {
                Console.WriteLine("Your Number is negative");
            }
            Console.ReadKey();
        }
    }
}
