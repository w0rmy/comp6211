﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise07
{
    class Program
    {
        static void Main(string[] args)
        {
            int n1;
            int n2;
            int temp;

            Console.WriteLine("Enter first number");
            n1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter second number");
            n2 = int.Parse(Console.ReadLine());
            Console.WriteLine($"Number1 is: {n1}. Number 2 is: {n2}");
            temp = n1;
            n1 = n2;
            n2 = temp;
            Console.WriteLine($"After swap number1 is now: {n1}. Number 2 is: {n2}");
            Console.ReadKey();
        }
    }
}
