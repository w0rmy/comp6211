﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise06
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please write the radius of your circle");
            double r = double.Parse(Console.ReadLine());
            double pi = Math.PI;
            double area = pi * (r * r);
            double p = 2 * pi * r;
            Console.WriteLine($"The perimeter of your circle is: {p}");
            Console.WriteLine($"The area of your circle is: {area}");
            Console.ReadLine();
        }
    }
}
