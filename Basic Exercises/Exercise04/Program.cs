﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise04
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 0;
            int b = 0;
            Console.WriteLine("Input first number");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input second number");
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"{a} x {b} = {a * b}");
            Console.ReadKey();
        }
    }
}
