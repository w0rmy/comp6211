﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ray with Ray
            int[] numbers = { 34, 72, 13, 44, 25, 30, 10, 11, 22, 33 };
            
            // Copy Ray to new Ray... Clone ray!
            int[] temp_numbers = numbers;

            // Create Array of Names
            string[] names = { "Jeremy", "Jacob", "Sam", "Belinda", "Steven", "Johnny", "Andrew", "Mark", "Ray", "Regan" };
            
            // Output Original Ray
            Console.Write("Original Array: ");

            foreach (int i in numbers)
            {
                Console.Write(i + " ");
            }
            
            // Reverse the Ray
            Console.WriteLine();
            Console.Write("Reverse Array: ");
            Array.Reverse(temp_numbers);
            foreach (int i in temp_numbers)
            {
                Console.Write(i + " ");
            }
            
            // Sort out Ray
            Console.WriteLine();
            Console.Write("Sort Array: ");
            Array.Sort(temp_numbers);
            foreach (int i in temp_numbers)
            {
                Console.Write(i + " ");
            }
            
            // Get Ray Lengh
            Console.WriteLine();
            Console.Write($"Array Length is: {numbers.Length}");

            // Get Ray Type
            Console.WriteLine();
            Type type1 = numbers.GetType().GetElementType();
            Console.Write($"Array 1 Type is: {type1}");

            // get variable at 5th index
            Console.WriteLine();
            Console.Write($"Variable at index 5 is: {numbers[5]}");

            // Copy an array another way
            Console.WriteLine();
            string[] names_new = new string[10];
            Array.Copy(names, names_new, 10);
            foreach (string n in names_new)
            {
                Console.Write(n + " ");
            }




            // Stop application exit until keystroke
            Console.ReadKey();
        }
    }
}
